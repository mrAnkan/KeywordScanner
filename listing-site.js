/**
 * Source Duck
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

const $ = require('cheerio');

const http = require('./http');
const helpers = require('./helpers');

const Listing = require('./listing');

module.exports = class ListingSite {
  
  constructor(
    name, 
    domain, 
    uri, 
    article_list_uri_selector, 
    article_list_headline_selector, 
    article_headline_selector, 
    article_content_selector,
    article_source_links_selector) {
    
    this.name = name;
    this.domain = domain;
    this.uri = uri; 
    this.article_list_uri_selector = article_list_uri_selector; 
    this.article_list_headline_selector = article_list_headline_selector;
    this.article_headline_selector = article_headline_selector;
    this.article_content_selector = article_content_selector;
    this.article_source_links_selector = article_source_links_selector;

    this.listings = [];
  }

  async fetchListings(perform) {  
    const current = await http.download(this.uri);

    $(this.article_list_uri_selector, current.content)
      .each((_index, element) => {
        const currentLink = $(element).attr('href');
        const formatLink = helpers.formatLink(currentLink, this.domain);
        const headline = helpers.findHeadline(element);

        const listing = new Listing(formatLink, headline);
        this.listings.push(listing);

        perform(listing);
      });
  }

  async fetchArticles(perform) {
    await Promise.all(this.listings.map(async listing => {
      await listing.fetchArticle(
        this.article_headline_selector, 
        this.article_content_selector, 
        this.article_source_links_selector,
        () => {
          perform(listing);
        }
      );
    }));
  }

  async fetchSources(perform) {
    await Promise.all(this.listings.map(async listing => {
      await Promise.all(listing.article.outbound_sources.map(async source => {
        await listing.article.fetchSource(source);
      }));
      await Promise.all(listing.article.inbound_sources.map(async source => {
        await listing.article.fetchSource(source);
      }));

      perform(listing)
    }));
  }
}