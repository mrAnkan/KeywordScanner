/**
 * Source Duck
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

const shell = require('electron').shell;

module.exports = class ListingSiteRenderer {

  constructor() {
    this.main = document.getElementsByTagName('main')[0];

    this.templates = {
      article_section: document.getElementById('article-section'),
      article_entry: document.getElementById('article-entry'),
      link_entry: document.getElementById('link-entry')
    }    
  }

  async renderSite(site) {
    this.appendSection(site.domain, site.name);

    await site.fetchListings(listing => {
      this.appendArticle(listing, site.domain);
    });

    await site.fetchArticles(listing => {
      for (const source of listing.article.outbound_sources) {
        this.appendSource(source.link, 'outbound-sources', source.id, listing.id);
      }
      for (const source of listing.article.inbound_sources) {
        this.appendSource(source.link, 'inbound-sources', source.id, listing.id);
      }
    });

    await site.fetchSources(listing => {
      for (const source of listing.article.outbound_sources) {
        this.updateSource(source.link, source.headline, source.id);
      }
      for (const source of listing.article.inbound_sources) {
        this.updateSource(source.link, source.headline, source.id);
      }
    });
  }

  appendSection(id, name) {
    const section = this.templates.article_section.content.firstElementChild.cloneNode(true);
    section.id = `section-${id}`;
    
    section.querySelectorAll('h1')[0].innerHTML = name;

    this.main.appendChild(section);
  }

  appendArticle(listing, domain) {
    const article = this.templates.article_entry.content.firstElementChild.cloneNode(true);
    const link = article.querySelectorAll('.main-summarizing')[0];
    
    article.id = `article-${listing.id}`;

    link.title = listing.link;
    link.href = listing.link;
    link.innerHTML = listing.headline;

    link.addEventListener('click', (event) => {
      event.preventDefault();
      shell.openExternal(listing.link);
    })

    const section = document.getElementById(`section-${domain}`);

    section.appendChild(article);
  }

  appendSource(link, type, id, articleId) {
    const holder = this.templates.link_entry.content.firstElementChild.cloneNode(true);
    const linkElement = holder.querySelectorAll('a')[0];

    linkElement.id = `source-${id}`;
    linkElement.title = link;
    linkElement.href = link;

    linkElement.addEventListener('click', (event) => {
      event.preventDefault();
      shell.openExternal(link);
    })

    const article = this.main.querySelector(`#article-${articleId}`);
    const sourceDiv = article.querySelectorAll(`.${type}`)[0];
    
    sourceDiv.appendChild(holder);
  }

  updateSource(link, headline, id) {
    const linkElement = document.getElementById(`source-${id}`);
    linkElement.title = link;
    linkElement.href = link;
    linkElement.innerHTML = headline;
    linkElement.parentElement.classList.remove('loading');
  }
}
