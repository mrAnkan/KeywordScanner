/**
 * Source Duck
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';
const Promise = require('bluebird');

const {http, https} = require('follow-redirects');

var exports = module.exports = {}

/**
 * Download a html or json formatted resource/ page.
 * @param {string} uri The uri of resource to download.
 */
exports.download = function (uri) {
  return new Promise(function (resolve, reject) {
    https.get(uri, (result) => {

      // Easy access to status and type.
      const statusCode = result.statusCode;
      const contentType = result.headers['content-type'];

      let error;
      if (statusCode !== 200) {
        error = new Error(`Request to ${uri} Failed.\n Status Code: ${statusCode}`);
      }

      if (error) {
        console.error(`Request to ${uri} Failed.\n Error: ${error.message}`);
        console.error(error);
  
        // Consume response data to free up memory.
        result.resume();

        reject(error);
      }

      let rawData = '';
      result.on('data', (chunk) => {
        // Store data in var.
        rawData += chunk;
      });

      result.on('end', () => {
        const data = {
          'uri': uri,
          'content': rawData, 
          'contentType': contentType
        }

        // Pass back content and the type of content.
        resolve(data);
      });
    }).on('error', (outerError) => {
      console.error(`Request to ${uri} Failed.\n Error: ${outerError.message}`);
      console.error(outerError);

      reject(outerError);
    });
  });
}
