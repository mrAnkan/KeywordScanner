/**
 * Source Duck
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

const http = require('./http');
const helpers = require('./helpers');
const $ = require('cheerio');
const uuid = require('uuid-v4');

const Article = require('./article');
const Source = require('./source');

module.exports = class Listing {
  
  constructor(link, headline) {
    this.link = link;
    this.headline = headline;
    this.id = uuid();

    this.article = null;
  }

  async fetchArticle(headline_selector, content_selector, source_links_selector, perform) {

    if(!this.article) {
      const listingLinkDetails = helpers.getLinkDetails(this.link);
      const current = await http.download(this.link);
      
      const headline = $(headline_selector, current.content)[0];
      const content = $(content_selector, current.content)[0];

      this.article = new Article(
        this.link, 
        helpers.extractTrimmedText(headline) ?? this.headline, 
        content
      );

      $(source_links_selector, current.content)
        .each((_index, element) => {
          const currentLinkDetails = helpers.getLinkDetails($(element).attr('href'));
          const linkText = helpers.extractTrimmedText(element);
          const prefix = (currentLinkDetails.domain == listingLinkDetails.domain) ? 'in' : 'out';

          this.article[`${prefix}bound_sources`].push(new Source(currentLinkDetails.formattedLink, linkText));
      });  
    }

    perform(this.article);
  }
}