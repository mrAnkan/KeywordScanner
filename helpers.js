/**
 * Source Duck
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
const $ = require('cheerio');

var exports = module.exports = {}

/**
 * Wait for callbacks to perform and collect their results.
 * @param {Array} callbacks An array of functions to perform.
 */
exports.gatherResults = async function (callbacks) {
  const results = [];

  for (const callback of callbacks) {
    results.push(await callback)
  }

  return results;
}

/**
 * Format link to have prefix and hostname if missing.
 * @param {string} link A link, entered in a somewhat unknown format.
 * @param {string} domain The domain/hostname that should be present in the uri.
 */
exports.formatLink = function(link, domain) {
  if (!link)
    return link

  let replacement = 'https://';

  if (!link.includes(domain))
    replacement += `${domain}/`;

  if (!link.startsWith('http')) {
    if (link.startsWith("//")) {
      link = link.replace('//', replacement);
    } else if (link.startsWith('/')) {
      link = link.replace('/', replacement);
    }
  }

  return link;
}

/**
 * Get details about provided uri using an a-tag.
 * @param {string} uri The uri to extract details from.
 */
exports.getLinkDetails = function(uri) {
  var a = document.createElement('a');
  a.href = uri;
  
  return {
    protocol: a.protocol,
    hostname: a.hostname,
    domain: this.extractDomainFromHost(a.hostname), 
    port: a.port,
    pathname: a.pathname,
    search: a.search,
    hash: a.hash,
    host: a.host,
    link: uri,
    formattedLink: this.formatLink(uri, this.extractDomainFromHost(a.hostname)) 
  }
}

/**
 * Extract the main domain from hostname.
 * @param {string} string Hostname from an html a-tag.
 */
exports.extractDomainFromHost = function(string) {
  const last = string.lastIndexOf('.');
  const part = string.substring(0, last);
  const lastSub = part.lastIndexOf('.');

  return lastSub > 0 ? string.substring(lastSub + 1, string.length) : string;
}

/**
 * Find the first child that is a headline (h1-h6).
 * @param {element} element The element to find the first headline within.
 */
exports.findHeadline = function(element) {
  for(let i = 1; i <= 6; i++) {
    const current = $(element).find(`h${i}`);

    if(current.length > 0)
      return $(current).text().trim();
  }

  return $(element).text().trim();
}

/**
 * Extract trimmed text from an element.
 * @param {*} element The element to extract trimmed text from.
 */
exports.extractTrimmedText = function(element) {
  if(!element)
    return null;
  return $(element).text().trim();
}