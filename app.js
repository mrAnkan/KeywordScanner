/**
 * Source Duck
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

const ListingSiteRepository = require('./listing-site-repository');
const TabsRepository = require('./tabs-repository');
const ListingSiteRenderer = require('./listing-site-renderer');
const NavigationRenderer = require('./navigation-renderer');

module.exports = class SourceDuck {
  constructor(db) {
      this.db = db;
      this.listingSiteRenderer = new ListingSiteRenderer();
      this.navigationRenderer = new NavigationRenderer();

      this.listingSiteRepository = new ListingSiteRepository(this.db);
      this.tabsRepository = new TabsRepository(this.db);
  }

  async initialize() {
    await this.listingSiteRepository.initialize();
    await this.navigationRenderer.initialize();
    await this.tabsRepository.initialize();

    if(this.tabsRepository.activeTab) {
      const site = listingSiteRepository.getDetailsById(tabsRepository.activeTab.siteId);
      this.navigationRenderer.appendTab(site.domain, site.name, site.uri);
  
      await this.listingSiteRenderer.renderSite(site);
    } else {
      this.navigationRenderer.openAddTab();
    }
  }
}
