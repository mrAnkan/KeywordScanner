/**
 * Source Duck
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

const http = require('./http');
const helpers = require('./helpers');
const $ = require('cheerio');

module.exports = class Article {
  
  constructor(link, headline, content) {
    this.link = link;
    this.headline = headline;
    this.content = content;

    this.outbound_sources = [];
    this.inbound_sources = [];
  }

  async fetchSource(source) {
    try {
      const current = await http.download(source.link);

      source.content = $('main', current.content)[0];
      source.headline = helpers.extractTrimmedText($('title', current.content)[0]);
    } catch (error) {
      console.error(error);
    }
  }
}