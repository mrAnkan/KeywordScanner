/**
 * Source Duck
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const ListingSite = require("./listing-site");

module.exports = class ListingSiteRepository {

  constructor(db) {
    this.db = db;
    this.mappedByUri = {};
    this.mappedByDomain = {};
    this.mappedById = {};
    this.uris = [];
    this.listed = [];
  }

  /**
   * Load data about sites from database to memory.
   */
  async initialize() {
    const db = this.db;
    const [sites] = await Promise.all([
      db.all('SELECT * FROM sites')
    ]);
  
    for (const site of sites) {
      const current = new ListingSite(
        site.name, 
        site.domain, 
        site.uri, 
        site.article_list_uri_selector, 
        site.article_list_headline_selector, 
        site.article_headline_selector, 
        site.article_content_selector,
        site.article_source_links_selector
      );

      this.mappedByUri[site.uri] = current;
      this.mappedByDomain[site.domain] = current;
      this.mappedById[site.id] = current;
      this.uris.push(site.uri);
      this.listed.push(current);
    }
  } 

  /**
   * Get selector path to be used for content extraction.
   * @param {string} domain The domain to get selector for.
   * @param {string} selector The specific selector to get from memory.
   */
  getSelector(domain, selector) {
    return this.mappedByDomain[domain] ? this.mappedByDomain[domain][`${selector}_selector`] : null; 
  }

  /**
   * Get a list of uris from memory.
   */
  getListingSitesUris() {
    return this.uris;
  }

  /**
   * Get a list of sites from memory.
   */
  getListingSites() {
    return this.listed;
  }

  /**
   * Get details associated with specific id.
   * @param {string} id The db id of site to get details for.
   */
  getDetailsById(id) {
    return this.mappedById[id];
  }

  /**
   * Get details associated with specific uri.
   * @param {string} uri The uri to get details for.
   */
  getDetailsByUri(uri) {
    return this.mappedByUri[uri];
  }

  /**
   * Get details associated with specific domain.
   * @param {string} domain The domain to get details for.
   */
  getDetailsByDomain(domain) {
    return this.mappedByDomain[domain];
  }
}