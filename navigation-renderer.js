/**
 * Source Duck
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

module.exports = class NavigationRenderer {
  constructor(focusAddTab){
    this.elements = {
      tabs: document.getElementById('tabs'),
      link: document.getElementById('add-tab-action'),
      img: document.getElementById('add-tab-action-img'),
      input: document.getElementById('add-tab-input')
    }

    this.templates = {
      tab_entry: document.getElementById('tab-entry'),
    }

    this.focusAddTab = !focusAddTab;
  }

  async initialize() {
    this.elements.link.addEventListener('click', (event) => {
      event.preventDefault();
      this.setFocusAddTab();
    })

    this.elements.input.addEventListener('keyup', async (event) => {
      event.preventDefault();
      if (event.keyCode === 13) {
        this.performSearch(event.target.value);
      }
    })
  }

  setFocusAddTab() {
    if(this.focusAddTab) {
      this.closeAddTab();
      this.focusAddTab = false;
    } else {
      this.openAddTab();
      this.focusAddTab = true;
    }
  }

  async performSearch(value) {
    const site = await window.app.listingSiteRepository.getDetailsByDomain(value);
        
    if(site) {
      this.setFocusAddTab();
      this.appendTab(site.domain, site.name, site.uri);

      await window.app.listingSiteRenderer.renderSite(site);
    }
  }

  appendTab(domain, name, link) {
    const tab = this.templates.tab_entry.content.firstElementChild.cloneNode(true);
    const linkElement = tab.querySelectorAll('.tab-link')[0];
    linkElement.title = link;
    linkElement.innerHTML = name;
    tab.id = `tab-${domain}`;
    tab.classList.add('active');

    const closeElement = tab.querySelectorAll('.tab-close-link')[0];
    
    closeElement.addEventListener('click', (event) => {
      event.preventDefault();
      tab.parentElement.removeChild(tab);

      const section = document.getElementById(`section-${domain}`);
      section.classList.add('hidden');
    })

    this.elements.tabs.appendChild(tab);
  }
  
  openAddTab() {
    this.elements.input.classList.remove('hidden');
    this.elements.input.focus();

    this.elements.link.title = 'Close Input';
    this.elements.img.src = './img/x.svg'
    this.elements.img.alt = 'x';
  }

  closeAddTab() {
    this.elements.input.classList.add('hidden');
    this.elements.input.blur();

    this.elements.link.title = 'Open Listing Site in New Tab';
    this.elements.img.src = './img/plus.svg'
    this.elements.img.alt = '+';
  }
}